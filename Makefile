POSTGRES_USER ?= postgres
PATH_TO_SEED ?= ./db/multadb.sql

setup:
	psql -U $(POSTGRES_USER) -c "create database multadb;"
	psql multadb < $(PATH_TO_SEED)
	npm install

run:
	POSTGRES_USER=$(POSTGRES_USER) npm start


