# Multabell

# Requirements
* [Node 6.9.5](https://nodejs.org/es/)
* [PostgreSQL@9.6.2]

# Setup

By default it use the "postgres" user. You can change it by passing
the environment variable "POSTGRES_USER"


By default the location of the seed sql file is in "./db/multadb.sql".
You can change it by passing the environment variable "PATH_TO_SEED"

```bash
$ make setup -e POSTGRES_USER="<your_user>" -e PATH_TO_SEED="<path_to_seed.sql>"
```

# Run

By default the postgres user is "postgres". You can change it by passing the
environment variable "POSTGRES_USER"

```bash
$ make run -e POSTGRES_USER="<your_user"
```