const { Pool } = require('pg');

const pool = new Pool({
    user: process.env.POSTGRES_USER,
    host: 'localhost',
    port: 5432,
    database: 'multadb'
});

module.exports = {
    query: (text, params, callback) => {
        return pool.query(text, params, callback);
    }
}