var express = require('express');
var router = express.Router();

var request = require('request');
var cheerio = require('cheerio');

var platesModel = require('../../models/plates');
var ticketModel = require('../../models/tickets');

var url = require('url');

router.post('/', function(req, res, next) {
    
    const actionUrl = "http://servicios.monterrey.gob.mx/consultas/transito/Reportes/Infracciones.aspx";

    const headParams = {
        "Content-Type": "application/x-www-form-urlencoded"
    };

    const bodyParams = {
        "__EVENTVALIDATION": "/wEWAwKhu6KsDALIrvbuBALE/qnLDmEh3j3X0HyTgOGi4TX7HVDXd+j8",
        "__VIEWSTATE": "/wEPDwUKLTIxMjcwMzAyNmRk4E9fBVzudqbzGsCYlcQbNxEpohM=",
        "__VIEWSTATEGENERATOR": "B256B613",
        "ctl00$ContentPlaceHolder1$btnBuscar": "Buscar",
        "ctl00$ContentPlaceHolder1$txtNumPlaca": req.body.plate
    };

    request.post({headers: headParams, url: actionUrl, form: bodyParams}, function(err, response, body) {
        if (err) res.send(500);
        var $ = cheerio.load(body);

        var data = [];
        var processedData = [];

        $('table#ctl00_ContentPlaceHolder1_tblEdoCuenta td').each(function() {
            var cellText = $(this).html();
            data.push(cellText);
        });

        if (data.length > 6) {
            var chunk = 6;
            for (var i = 6; i < data.length; i+=chunk) {
                processedData.push(data.slice(i, i+6))
            }
            info = {multas: processedData, data: JSON.stringify(processedData)}
            res.render('results', info);
        } else {
            res.render('results');
        }
    });
});

router.post('/save', function(req, res, next) { 
    
    function convert (date) {
        const months = {
            "Jan": '01',
            "Feb": "02",
            "Mar": "03",
            "Apr": "04",
            "May": "05",
            "Jun": "06",
            "Jul": "07",
            "Aug": "08",
            "Sep": "09",
            "Oct": "10",
            "Nov": "11",
            "Dec": "12"
        }
        var newDate =  months[date.substring(3, 6)] + '/' + date.substring(0, 3) + date.substring(6, 12);
        return newDate;
    }

    platesModel.createPlate(JSON.parse(req.body.multas)[0][1], function(err) {
        if(err) {
            if (err.code === '23505') {
                console.error("Duplicated plate: ", err);
            } else {
                return res.render('error', {message: "ERROR while creating plate", error: err})
            }
        }
    });

    for (i in JSON.parse(req.body.multas)) {
        item = JSON.parse(req.body.multas)[i];
        item[2] = convert(item[2]);
        item[5] = parseFloat(item[5].substring(1).replace(',', ''));
        ticketModel.createTicket(item, function(err) {
            if (err) {
                if (err.code === '23505') {
                    console.error("Duplicated ticket: ", err);
                } else {
                    return res.render('error', {message: "ERROR while creating ticket", error: err});
                }
            }
        });
    }

    return res.redirect('/?save=true')
});

module.exports = router;